﻿using System;
using MySql.Data.MySqlClient;
using System.Net.Http;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json.Linq;
using System.Data;
using System.Globalization;

namespace mhi_gnnextract
{
    class Program
    {
        //static string d_name = "mhi-va.alfa-erp.com";
        static string d_name = "erp.mhi-va.com";
        static string c_date = DateTime.Now.ToString("yyyy-MM-dd");
        private static readonly HttpClient client = new HttpClient();
        static string st_key = "sfGa0kl7lO9fXWaE1rENp";

        static string id_4241 = "";
        static void Main(string[] args)
        {
            try
            {
               string connstring_setupdb = "Server = localhost; Database = setupsheetdb; Uid = root; Pwd = ;SslMode=none";
               MySqlConnection mcon_setup = new MySqlConnection(connstring_setupdb);
               if (mcon_setup.State == ConnectionState.Closed)
              {
                    mcon_setup.Open();
                }
                extract_process_info();
                call_bend(mcon_setup);
            }
            catch (Exception e)
            {
                FileInfo fInfo = new FileInfo(AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar + "log.txt");
                if (fInfo.Length > 2147483248)
                {
                    File.WriteAllText(fInfo.FullName, "");
                }
                using (StreamWriter w = File.AppendText("log.txt"))
                {
                    Log("" + e, w);
                }
            }
        }
        public static void extract_process_info()
        {
            string html = string.Empty;

            string pr_id_url = "https://" + d_name + "/api/get_alfaerp_settings/SCHEDULER?key=56ACtlLKKf8LasERZPyf&factory=hq";
            //EventViewer.WriteLog(pr_id_url);
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
           
            HttpWebRequest request1 = (HttpWebRequest)WebRequest.Create(pr_id_url);
            request1.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            using (HttpWebResponse response1 = (HttpWebResponse)request1.GetResponse())
            using (Stream stream1 = response1.GetResponseStream())
            using (StreamReader reader1 = new StreamReader(stream1))
            {
                html = reader1.ReadToEnd();
                dynamic val_response1 = JObject.Parse(html);

                if (val_response1.processes != null)
                {
                    for (int y = 0; y < val_response1.processes.Count; y++)
                    {
                        string processname = val_response1.processes[y].name;
                        if (processname == "4241")
                        {
                            id_4241 = val_response1.processes[y].id;
                            break;
                        }
                       
                    }
                }
            }

        }
        static void call_bend(MySqlConnection mcon_setup)
        {

            string html = string.Empty;
            
            html = string.Empty;
            DateTime date = DateTime.Parse(c_date);
            string fromdate = date.AddDays(-300).ToString("yyyy-MM-dd");
            //fromdate = "2021-06-01";
            string todate = date.AddDays(300).ToString("yyyy-MM-dd");
            //todate = "2021-09-30";
            string url = @"https://" + d_name + "/api/get_alfaerp_data/SCHEDULERORDERV2?key=" + st_key + "&pids="+ id_4241+ "&page=0&limit=1000&ps=0&sv=&ds=" + fromdate + "&de=" + todate + "&df==deliveryDate,processDate";
            //EventViewer.WriteLog(url);
            ServicePointManager.Expect100Continue = true;
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                            //request.Headers["x-openerp-session-id"] = "";
                            request.AutomaticDecompression = DecompressionMethods.GZip | DecompressionMethods.Deflate;
            using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                            using (Stream stream = response.GetResponseStream())
                            using (StreamReader reader = new StreamReader(stream))
                            {
                                html = reader.ReadToEnd();

                                dynamic val_response = JObject.Parse(html);
                                int counter = 0;
                                int tot_count = val_response.count;
                //EventViewer.WriteLog(""+ tot_count);
                for (int i = 0; i < tot_count; i++)
                {
                    if (val_response.mos[i].itemList != null) { 
                     var cust_name = val_response.mos[i].customerName;
                    // if (cust_name == "MHI-VA") {

                    for (int j = 0; j < val_response.mos[i].itemList.Count; j++)
                    {

                        string smimodel = val_response.mos[i].itemList[j].smi_model_number;
                        string prodno = val_response.mos[i].itemList[j].productNo;
                        string mono = val_response.mos[i].itemList[j].smi_mo_number;
                        string qty = val_response.mos[i].itemList[j].lotSize;
                        string duedate = val_response.mos[i].itemList[j].dueDate;
                        string intduedate = val_response.mos[i].itemList[j].internal_due_date;
                        string matwidth = val_response.mos[i].itemList[j].materialWidth;
                        string matlength = val_response.mos[i].itemList[j].materialLength;
                        string thickness = val_response.mos[i].itemList[j].partThickness;
                        string matcode = val_response.mos[i].itemList[j].smi_alloy;
                        string matcode_val = val_response.mos[i].itemList[j].materialCode;
                        string punchno_val1 = val_response.mos[i].itemList[j].punch_no_1;
                            if (punchno_val1 == "0" || punchno_val1 == "" || punchno_val1 == " " || punchno_val1 == null)
                            {
                                punchno_val1 = "";
                            }
                            string dieno_val1 = val_response.mos[i].itemList[j].die_no_1;
                            if (dieno_val1 == "0" || dieno_val1 == "" || dieno_val1 == " " || dieno_val1 == null)
                            {
                                dieno_val1 = "";
                            }
                            string punchno_val2 = val_response.mos[i].itemList[j].punch_no_2;
                            if (punchno_val2 == "0" || punchno_val2 == "" || punchno_val2 == " " || punchno_val2 == null)
                            {
                                punchno_val2 = "";
                            }else
                            {
                                punchno_val2 = ","+ punchno_val2;
                            }

                            string dieno_val2 = val_response.mos[i].itemList[j].die_no_2;
                            if (dieno_val2 == "0" || dieno_val2 == "" || dieno_val2 == " " || dieno_val2 == null)
                            {
                                dieno_val2 = "";
                            }
                            else
                            {
                                dieno_val2 = "," + dieno_val2;
                            }
                            string serialcode = "";
                        if (val_response.mos[i].itemList[j].sheetSerialCode != null)
                        {
                            if (val_response.mos[i].itemList[j].sheetSerialCode != "")
                            {
                                serialcode = val_response.mos[i].itemList[j].sheetSerialCode;
                            }
                        }
                        string status = val_response.mos[i].itemList[j].processList[0].status;
                        string orderid = val_response.mos[i].itemList[j].processList[0].id;
                        string jobduedate = duedate;
                        string final_jobdate = duedate;
                        string final_intdate = duedate;

                        if (!String.IsNullOrEmpty(intduedate))
                        {
                            DateTime temp = DateTime.ParseExact(intduedate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                            final_intdate = temp.ToString("yyyy-MM-dd");
                        }
                        if (val_response.mos[i].itemList[j].processList.Count > 0)
                        {
                            jobduedate = val_response.mos[i].itemList[j].processList[0].endDate;
                            if (!String.IsNullOrEmpty(jobduedate))
                            {
                                DateTime temp = DateTime.ParseExact(jobduedate, "MM/dd/yy HH:mm:ss tt", CultureInfo.InvariantCulture);
                                final_jobdate = temp.ToString("yyyy-MM-dd");
                            }
                        }

                        string prodname = val_response.mos[i].itemList[j].productName;

                        string CmdText1 = "INSERT IGNORE INTO setupsheetmhi2 VALUES(@sid,@machinename,@materialname,@materialsize,@filename,@date,@processed,@sheets,@processingtime,@starttime,@endtime,@programname,@pduedate,@intduedate,@qty,@partnum,@compid,@partname,@comment,@orderno,@sheetcode,@toolinfo,@serialno,@priority,@pdfdownloaded,@smimodel,@orderid,@suspendval,@punchno,@daino)";
                        MySqlCommand cmd1 = new MySqlCommand(CmdText1, mcon_setup);
                        cmd1.Parameters.AddWithValue("@sid", mono + "$" + prodno);
                        cmd1.Parameters.AddWithValue("@machinename", "FLC3015AJ");
                        cmd1.Parameters.AddWithValue("@materialname", matcode);
                        cmd1.Parameters.AddWithValue("@materialsize", matwidth + "x" + matlength);
                        cmd1.Parameters.AddWithValue("@filename", "");
                        cmd1.Parameters.AddWithValue("@sheets", 1);
                        cmd1.Parameters.AddWithValue("@processingtime", "");
                        cmd1.Parameters.AddWithValue("@starttime", "0000-00-00 00:00:00");
                        cmd1.Parameters.AddWithValue("@endtime", "0000-00-00 00:00:00");
                        cmd1.Parameters.AddWithValue("@comment", matcode_val);
                        cmd1.Parameters.AddWithValue("@programname", mono);
                        cmd1.Parameters.AddWithValue("@partnum", prodno);
                        cmd1.Parameters.AddWithValue("@partname", prodname);
                        cmd1.Parameters.AddWithValue("@qty", qty);
                        cmd1.Parameters.AddWithValue("@orderno", mono);
                        cmd1.Parameters.AddWithValue("@compid", "841");
                        cmd1.Parameters.AddWithValue("@processed", 0);
                        cmd1.Parameters.AddWithValue("@pduedate", final_jobdate);
                        cmd1.Parameters.AddWithValue("@intduedate", final_intdate);
                        cmd1.Parameters.AddWithValue("@date", c_date);
                        cmd1.Parameters.AddWithValue("@sheetcode", thickness);
                        cmd1.Parameters.AddWithValue("@toolinfo", "");
                        cmd1.Parameters.AddWithValue("@serialno", serialcode);
                        cmd1.Parameters.AddWithValue("@priority", 0);
                        cmd1.Parameters.AddWithValue("@pdfdownloaded", 1);
                        cmd1.Parameters.AddWithValue("@smimodel", smimodel);
                        cmd1.Parameters.AddWithValue("@orderid", orderid);
                        cmd1.Parameters.AddWithValue("@suspendval", 0);
                        cmd1.Parameters.AddWithValue("@punchno", punchno_val1+ punchno_val2);
                        cmd1.Parameters.AddWithValue("@daino", dieno_val1+ dieno_val2);
                        if (status.Equals("ready"))
                        {
                            int result = cmd1.ExecuteNonQuery();
                            if (result == 1)
                            {
                                EventViewer.WriteLog(mono + "$" + prodno);
                            }
                            /*else
                            {
                                    string query = string.Format(@"UPDATE `setupsheetdb`.setupsheetmhi2 SET sheetcode='{1}' where sid='{0}'", mono + "$" + prodno, thickness);
                                    using (MySqlCommand command = new MySqlCommand(query, mcon_setup))
                                    {
                                        int row = command.ExecuteNonQuery();
                                        if (row == 1)
                                        {
                                            EventViewer.WriteLog("Updated2--" + mono + "$" + prodno+"----"+thickness);
                                        }
                                    }
                                }*/
                            }
                            /*if (status.Equals("done"))
                            {
                                string query = string.Format(@"UPDATE `setupsheetdb`.setupsheetmhi2 SET sheetcode='{1}' where sid='{0}'", mono + "$" + prodno, thickness);
                                using (MySqlCommand command = new MySqlCommand(query, mcon_setup))
                                {
                                    int row = command.ExecuteNonQuery();
                                    if (row == 1)
                                    {
                                        EventViewer.WriteLog("Updated2--" + mono + "$" + prodno + "----" + thickness);
                                    }
                                }
                            }
                                 else
                                 {
                                     string query = string.Format(@"UPDATE `setupsheetdb`.setupsheetmhi2 SET intduedate='{1}' where sid='{0}'", mono + "$" + prodno, final_intdate);


                                     using (MySqlCommand command = new MySqlCommand(query, mcon_setup))
                                     {
                                         int row = command.ExecuteNonQuery();
                                         if (row == 1)
                                         {
                                             EventViewer.WriteLog("Updated2--" + mono + "$" + prodno);
                                         }
                                     }
                                 }*/
                        }
                    }
                                    counter++;
                                    // }

                                }
                            }
                        
        }
        public static string SaveFile(Stream stream, string filename)
        {
            string filePath = System.IO.Path.Combine(@"C:\setap100\", filename);
            try
            {
                using (var filestr = System.IO.File.Create(filePath))
                {
                    stream.CopyTo(filestr);
                    filestr.Flush();
                }
                stream.Dispose();
                return filePath;
            }
            catch (Exception e)
            {
                EventViewer.WriteLog("OFcPath : Failed" + e.StackTrace + " ");
            }
            return null;
        }
        public static void Log(string logMessage, TextWriter w)
        {
            w.Write("\r\nLog Entry : ");
            w.WriteLine($"{DateTime.Now.ToLongTimeString()} {DateTime.Now.ToLongDateString()}");
            w.WriteLine("  :");
            w.WriteLine($"  :{logMessage}");
            w.WriteLine("-------------------------------");
        }
        static List<string> getFileIds()
        {
            String sqlFormattedDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            string file_id_log = AppDomain.CurrentDomain.BaseDirectory + System.IO.Path.DirectorySeparatorChar + "fileid_log_" + sqlFormattedDate + ".txt";
            List<string> lines = new List<string>();
            using (StreamReader sr = File.OpenText(file_id_log))
            {
                string[] lines_arr = File.ReadAllLines(file_id_log);
                for (int x = 0; x <= lines_arr.Length - 1; x++)
                {
                    lines.Add(lines_arr[x]);
                }

            }
            return lines;
        }
        public static bool isFileIdExists(List<string> ids, string id)
        {
            bool isMatch = false;
            if (ids != null && ids.Count > 0)
            {
                isMatch = ids.Any(s => s.Contains(id));
            }
            return isMatch;
        }
        
    }
}
