﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;

namespace mhi_gnnextract
{
    class EventViewer
    {
        static string logFile = AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar + "log_content.txt";
       
        public static void WriteLog(Exception ex)
        {
            try
            {
                string msg = DateTime.Now.ToString() + ": " + ex.Message.ToString().Trim() + Environment.NewLine + ex.StackTrace.ToString().Trim();
                FileInfo fInfo = new FileInfo(logFile);
                if (fInfo.Length > 2147483248)
                {
                    File.WriteAllText(fInfo.FullName, "");
                }
                File.AppendAllText(logFile, msg + Environment.NewLine);
            }
            catch
            {

            }
        }

        public static void WriteLog(string message)
        {
            try
            {
                FileInfo fInfo = new FileInfo(logFile);
                if (fInfo.Length > 2147483248)
                {
                    File.WriteAllText(fInfo.FullName, "");
                }
                File.AppendAllText(logFile, DateTime.Now.ToString() + ": " + message + Environment.NewLine);
            }
            catch
            {

            }
        }
        public static void WriteFileLog(string message)
        {
            String sqlFormattedDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            string file_id_log = AppDomain.CurrentDomain.BaseDirectory + Path.DirectorySeparatorChar + "fileid_log_" + sqlFormattedDate + ".txt";

            try
            {
                if (message.Equals(""))
                {

                    File.AppendAllText(file_id_log, message);
                }
                else
                {

                    File.AppendAllText(file_id_log, message + Environment.NewLine);
                }
            }
            catch
            {

            }
        }

        public static void WriteDebugLog(string message)
        {
            try
            {
                File.AppendAllText(logFile, "DEBUG MODE:" + Environment.NewLine + DateTime.Now.ToString() + ": " + message + Environment.NewLine);
            }
            catch
            {
                
            }
        }
    }
}
