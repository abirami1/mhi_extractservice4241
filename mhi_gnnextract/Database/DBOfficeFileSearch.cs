﻿using AlfaDockSheetExtractionService;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AlfaDockSheetExtractionService
{
    class DBOfficeFileSearch
    {

        public void UpdateApp100Setup_itext(string macName, string matName, string matSize, string partName, string qty, int sid, string pname, int nsheets, string procestime, string pddate,string compid,string ddate,string fname,string thumbguid, string guid)
        {
            int cid = Convert.ToInt32(compid);
            string updateAp100SetupQuery;
            if (partName.Length > 450)
            {
                partName = partName.Substring(0, 448);
            }
            if (pddate.Equals("0000-00-00"))
            {
                updateAp100SetupQuery = string.Format("UPDATE Ap100_Setup SET  mach_name=N'{0}' , mat_name='{1}' ,mat_size='{2}',partnum=N'{3}',qty='{4}',prog_name='{6}',numofsheets={7},processtime='{8}',scompid={9},setup_date='{10}',setup_filename='{11}',setup_guid='{12}',setup_thumbguid='{13}' WHERE sid={5}", macName, matName, matSize, partName, qty, sid, pname, nsheets, procestime,cid,ddate,fname,guid,thumbguid);
            }
            else
            {
                updateAp100SetupQuery = string.Format("UPDATE Ap100_Setup SET  mach_name=N'{0}' , mat_name='{1}' ,mat_size='{2}',partnum=N'{3}',qty='{4}',prog_name='{6}',numofsheets={7},processtime='{8}',pduedate='{9}',scompid={10},setup_date='{11}',setup_filename='{12}',setup_guid='{13}',setup_thumbguid='{14}' WHERE sid={5}", macName, matName, matSize, partName, qty, sid, pname, nsheets, procestime, pddate,cid, ddate, fname, guid, thumbguid);

            }
            using (var con = new SqlConnection(DBContext.ConnectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(updateAp100SetupQuery, con))
                    {
                        int rows = command.ExecuteNonQuery();

                        if (rows == 0)
                        {
                            InsertApp100Setup_itext(macName, matName, matSize, sid, pname, nsheets, procestime, pddate, partName,qty,cid,ddate,fname,guid,thumbguid);
                        }

                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception during updateApp100setup socket file " + ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
            }
        }
        public void InsertApp100Setup_itext(string macName, string matName, string matSize, int sid, string pname, int nsheets, string procestime, string pduedate, string partnum, string qty,int cid,string ddate, string fname, string guid, string thumbguid)
        {
            string addAp100SetupQuery;
            if (pduedate.Equals("0000-00-00"))
            {
                addAp100SetupQuery = string.Format("insert into Ap100_Setup(sid,mach_name,mat_name,mat_size,prog_name,numofsheets,processtime,partnum,qty,scompid,setup_date,setup_filename,setup_guid,setup_thumbguid) values({0},N'{1}',N'{2}'," +
              "N'{3}',N'{4}',{5},N'{6}',N'{7}',{8},{9},'{10}',N'{11}','{12}','{13}');  SELECT SCOPE_IDENTITY();", sid, macName, matName, matSize, pname, nsheets, procestime, partnum,qty,cid,ddate,fname,guid,thumbguid);
            }
            else
            {
                addAp100SetupQuery = string.Format("insert into Ap100_Setup(sid,mach_name,mat_name,mat_size,prog_name,numofsheets,processtime,pduedate,partnum,qty,scompid,setup_date,setup_filename,setup_guid,setup_thumbguid) values({0},N'{1}',N'{2}'," +
         "N'{3}',N'{4}',{5},N'{6}',N'{7}',N'{8}',{9},{10},'{11}',N'{12}','{13}','{14}');  SELECT SCOPE_IDENTITY();", sid, macName, matName, matSize, pname, nsheets, procestime, pduedate, partnum,qty,cid,ddate, fname, guid, thumbguid);

            }
            using (var con = new SqlConnection(DBContext.ConnectionString))
            {
                con.Open();
                try
                {
                    using (SqlCommand command = new SqlCommand(addAp100SetupQuery, con))
                    {
                        int rows = command.ExecuteNonQuery();
                        //command.ExecuteScalar();
                    }
                }
                catch (Exception ex)
                {
                    Console.WriteLine("Exception during insertApp100setup socket file " + ex.Message);
                    Console.WriteLine(ex.StackTrace);
                }
            }
        }
        static List<string> getFileIds()
        {
            String sqlFormattedDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            string file_id_log = AppDomain.CurrentDomain.BaseDirectory + System.IO.Path.DirectorySeparatorChar + "fileid_log_" + sqlFormattedDate + ".txt";
            List<string> lines=new List<string>();
            using (StreamReader sr = File.OpenText(file_id_log))
            {
                string[] lines_arr = File.ReadAllLines(file_id_log);
                for (int x = 0; x <= lines_arr.Length - 1; x++)
                {
                    lines.Add(lines_arr[x]);
                }
               
            }
            return lines;
        }
        Boolean isFileIdExists (List<string> ids, string id )
        {
            Boolean isMatch = false;
            if (ids != null && ids.Count>0)
            {
                isMatch = ids.Any(s => s.Contains(id));
            }
                return isMatch;
        }
        public void getOpenItems()
        {
            if (Global.timer.Enabled)
                Global.timer.Stop();

            String yesdayDate = DateTime.UtcNow.Date.AddDays(-1).ToString("yyyy-MM-dd");
            string Yesdayfile_path = AppDomain.CurrentDomain.BaseDirectory + System.IO.Path.DirectorySeparatorChar + "fileid_log_" + yesdayDate + ".txt";
            if (File.Exists(Yesdayfile_path))
            {
                File.Delete(Yesdayfile_path);
            }
                String sqlFormattedDate = DateTime.UtcNow.ToString("yyyy-MM-dd");
            EventViewer.WriteFileLog("");

            List<string> fIds = getFileIds();
            EventViewer.WriteLog("fetching db");

            using (var con = new SqlConnection(DBContext.ConnectionString))
            {
                try
                {
                    QueueManager queueInstance = QueueManager.Instance;
                    EventViewer.WriteLog("current items count : " + queueInstance.getItemsCount());

                    con.Open();

                     sqlFormattedDate = "2021-06-01";
                 // string query = @"SELECT Id,guid,compid,filename,date,thumbguid from SocketFiles with(nolock) WHERE deleted = 0 AND type='AP100_SETUP' and filetype = 1 AND compid IN(131,202,654,648,651,498,556,31,114,489,336,34,12,473,123,287,102,260,239,17,117,373,4,203,601,808,493) AND cast(date as date) = '" + sqlFormattedDate + "' ORDER BY Id DESC";
                    string query = @"SELECT Id,guid,compid,filename,date,thumbguid from SocketFiles with(nolock) WHERE deleted = 0 AND type='AP100_SETUP' and filetype = 1 AND compid IN(202) AND cast(date as date) = '" + sqlFormattedDate + "' ORDER BY Id DESC";

                   // string query = @"SELECT Id,guid,compid,filename from SocketFiles WHERE   deleted = 0 AND type='AP100_SETUP' AND (compid = 373) AND CONVERT(VARCHAR(25), date, 126) LIKE '" + sqlFormattedDate+"%' ORDER BY date DESC";

                    using (SqlCommand command = new SqlCommand(query, con))
                    {
                        SqlDataReader reader = command.ExecuteReader();
                        int count = 0;
                        if (reader.HasRows)
                        {
                            while (reader.Read())
                            {
                                //Console.WriteLine(reader["guid"].ToString());
                                Upload.data data = new Upload.data();

                                if (!reader.IsDBNull(reader.GetOrdinal("guid")))
                                    data.guid = reader["guid"].ToString();

                                if (!reader.IsDBNull(reader.GetOrdinal("thumbguid")))
                                    data.thumbguid = reader["thumbguid"].ToString();

                                if (!reader.IsDBNull(reader.GetOrdinal("date")))
                                {
                                    string datestr = reader.GetDateTime(reader.GetOrdinal("date")).ToString();
                                    data.up_date = DateTime.Parse(datestr).Date.ToString("yyyy-MM-dd");
                                }

                                if (!reader.IsDBNull(reader.GetOrdinal("compid")))
                                    data.compid = reader["compid"].ToString();

                                if (!reader.IsDBNull(reader.GetOrdinal("filename")))
                                    data.filename = reader["filename"].ToString();




                                if (!reader.IsDBNull(reader.GetOrdinal("Id")))
                                    data.fileid = (Int32)reader["Id"];
                             string fileId_val= reader["Id"].ToString();
                                //if (!reader.IsDBNull(reader.GetOrdinal("type")))
                                data.type = "socket";

                                //  if (!reader.IsDBNull(reader.GetOrdinal("subtype")))
                                data.subtype = "AP100_SETUP";

                                 if (!isFileIdExists(fIds,fileId_val) && data.guid!=null)
                                        queueInstance.addItem(data);
                                
                                
                                count++;
                            }
                            EventViewer.WriteLog("db items- "+ count.ToString() + "--total queue items now  " + queueInstance.getItemsCount());
                        }
                        if (queueInstance.getItemsCount() <= 0)
                        {
                            EventViewer.WriteLog("db items- " + count.ToString() + "--total db items now  " + queueInstance.getItemsCount());

                            if (!Global.timer.Enabled)
                                Global.timer.Enabled = true;
                        }
                    }

                }
                catch (Exception ex)
                {
                    if (!Global.timer.Enabled)
                        Global.timer.Enabled = true;
                    EventViewer.WriteLog("Exception in getOpenItems - Msg -" + ex.Message + Environment.NewLine + "Stacktrace -" + ex.StackTrace);

                }
                finally
                {
                    if (con != null) con.Close();
                }
            }
        }
    }
}
